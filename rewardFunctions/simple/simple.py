import math
def reward_function(params):
    '''
    Example of rewarding the agent to follow center line
    '''
    
    track_width = params['track_width']
    edge_buffer = 0.08 * track_width
    distance_from_center = params['distance_from_center'] 
    all_wheels_on_track = params['all_wheels_on_track']
    steering = abs(params['steering_angle'])
    speed = params['speed']
    progress = params['progress']
    
    # Calculate 3 markers that are at varying distances away from the center line
    edge_buffer = 0.08 * track_width
    distance_from_edge = 0.00 + distance_from_center - edge_buffer
    half_track = 0.5 * track_width
    # Steering penality threshold, change the number based on your action space setting
    ABS_STEERING_THRESHOLD = 15
    
    # Give higher reward if the car is closer to center line and vice versa
    reward = 1 - math.sqrt((abs(distance_from_edge)) / (half_track))
    
    #double reward for all wheels on track 
    if all_wheels_on_track :
        reward *= 2

    reward += (speed * 2)

    # Penalize reward if the agent is steering too much
    if steering > ABS_STEERING_THRESHOLD:
        reward *= 0.8

    if reward < 0:
        reward = 1e-3

    return float(reward)
    

