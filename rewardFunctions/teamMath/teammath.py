import math
def reward_function(params):
    '''
    Example of rewarding the agent to follow center line
    '''

    track_width = params['track_width']
    edge_buffer = 0.08 * track_width
    distance_from_center = params['distance_from_center']
    all_wheels_on_track = params['all_wheels_on_track']
    steering = abs(params['steering_angle'])
    speed = params['speed']
    progress = params['progress']
    is_left_of_center = params['is_left_of_center']
    waypoints = params['waypoints']
    closest_waypoints = params['closest_waypoints']
    heading = params['heading']

    # Calculate the direction of the center line based on the closest waypoints
    next_point = waypoints[closest_waypoints[1]]
    prev_point = waypoints[closest_waypoints[0]]

    # Calculate 3 markers that are at varying distances away from the center line
    edge_buffer = 0.1 * track_width
    distance_from_edge = abs(distance_from_center) - edge_buffer
    half_track = 0.5 * track_width

    # Give higher reward if the car is closer to center line
    reward = 1 - math.sqrt((abs(distance_from_edge)) / (half_track))

    # Calculate the direction in radius, arctan2(dy, dx), the result is (-pi, pi) in radians
    track_direction = math.atan2(next_point[1] - prev_point[1], next_point[0] - prev_point[0])
    # Convert to degree
    track_direction = math.degrees(track_direction)

    # Cacluate the difference between the track direction and the heading direction of the car
    direction_diff = abs(track_direction - heading)

    while direction_diff > 180:
        direction_diff = 360 - direction_diff

    # Modify the reward based on how close to the right heading
    DIRECTION_THRESHOLD = 20.0

    modify = (180 - direction_diff)/(180-DIRECTION_THRESHOLD)


    if direction_diff < DIRECTION_THRESHOLD:
        reward *= modify**12

    # Penalize reward if the agent is steering wrong way
    if_inside_and_left_turn = steering > 0 and is_left_of_center
    if_outside_and_right_turn = steering < 0 and (is_left_of_center == False)

    if if_inside_and_left_turn or if_outside_and_right_turn:
        #penalize
        reward *= 0.8
    else:
        #reward
        reward *= 1.5

    #double reward for all wheels on track
    if all_wheels_on_track :
        reward *= 2

    #weight the reward for greater speed
    reward += (speed * 2)

    if reward < 0:
        reward = 1e-3

    return float(reward)